#!/bin/bash

STAGING=staging
PRODUCTION=production
ENV_STAGING_FILE=.env.staging
ENV_PRODUCTION_FILE=.env.production

# MANUAL
USAGE="
\nNAME
\n\t$0
\n\nSYNOPSIS
\n\tbash $0 -h
\n\tbash $0 -e {$STAGING | $PRODUCTION}
\n\nDESCRIPTION
\n\t-a\tset up app environment
\n\t-h\tshow help text
"

# INPUT
while getopts ":he:" opt; do
  case $opt in
  e)
    APP_ENV=$OPTARG
    ;;
  h)
    echo -e $USAGE
    exit 0
    ;;
  \?)
    echo -e $USAGE
    ;;
  esac
done

# ERROR HANDLING
if [ -z "$APP_ENV" ]
then
  echo "$0: ERROR: app environment not specified"
  echo -e $USAGE
  exit
fi

if [ "$APP_ENV" != "$STAGING" ] && [ "$APP_ENV" != "$PRODUCTION" ]
then
  echo "$0: ERROR: invalid app environment"
  exit
fi

if [ "$APP_ENV" == "$STAGING" ] && [ ! -f "$ENV_STAGING_FILE" ]
then
  echo "$0: ERROR: $ENV_STAGING_FILE file not found"
  exit
fi

if [ "$APP_ENV" == "$PRODUCTION" ] && [ ! -f "$ENV_PRODUCTION_FILE" ]
then
  echo "$0: ERROR: $ENV_PRODUCTION_FILE file not found"
  exit
fi

#OUTPUT
case $APP_ENV in
  $STAGING) cp -f $ENV_STAGING_FILE .env;;
  $PRODUCTION) cp -f $ENV_PRODUCTION_FILE .env;;
esac

OUTPUT+="\n"
OUTPUT+=DEBUGGING=enabled
OUTPUT+="\n"

echo -e $OUTPUT >> .env
echo "$0: SUCCESS: config variables have been added"