import axios from 'axios';
import { CognitoIdToken } from 'amazon-cognito-identity-js';
import moment from 'moment-timezone';
import RequestHelper from '../utils/RequestHelper';
import { Post } from '../ducks/posts/types';

type GetPostParams = {
  idToken: CognitoIdToken;
};

/** Get all posts */
export async function getPosts(params: GetPostParams) {
  const { idToken } = params;

  const options: RequestInit = {
    method: 'GET',
    headers: { Authorization: idToken.getJwtToken() },
  };

  return RequestHelper.fetch('post/all', options);
}

/** Create a post */
export async function createPost({
  idToken,
  title,
  description,
  image
}: {
  idToken: CognitoIdToken,
  title: string,
  description: string,
  image: string
}): Promise<Post> {
  const form = new FormData();

  form.append('title', title);
  form.append('description', description);

  const imagePath = image.split('/');
  const imageName = imagePath[imagePath.length - 1];
  const imageType = 'image/jpeg';

  form.append('image', {
    uri: image,
    name: imageName,
    type: imageType,
  });

  const timezone = moment.tz.guess();

  const options: RequestInit = {
    method: 'POST',
    headers: {
      'Content-Type': 'multipart/form-data',
      Authorization: idToken.getJwtToken(),
      Timezone: timezone,
    },
    body: form,
  };

  return RequestHelper.fetch('post/today', options);
}

/** Delete a post */
export async function deletePost({
  idToken,
  postId,
}: {
  idToken: CognitoIdToken,
  postId: string,
}): Promise<void> {
  const options: RequestInit = {
    method: 'DELETE',
    headers: {
      Authorization: idToken.getJwtToken(),
    },
  };

  return RequestHelper.fetch(`post/${postId}`, options);
}