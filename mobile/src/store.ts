import Config from 'react-native-config';
import createSagaMiddleware from 'redux-saga';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  configureStore,
  getDefaultMiddleware,
  combineReducers,
} from '@reduxjs/toolkit';
import { persistReducer, persistStore } from 'redux-persist';

import rootSaga from './sagas/rootSaga';
import { ENABLED } from './utils/constants';
import { reducer as postReducer } from './ducks/posts';
import { reducer as authReducer } from './ducks/auth';
import { reducer as alertReducer } from './ducks/alert';
import { reducer as loadingReducer } from './ducks/loading';

const rootReducer = combineReducers({
  auth: authReducer,
  alert: alertReducer,
  loading: loadingReducer,
  posts: postReducer,
});

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['auth'],
};

const reducer = persistReducer(persistConfig, rootReducer);

const sagaMiddleware = createSagaMiddleware();

const defaultMiddlewareConfig = {
  thunk: false,
  serializableCheck: false,
};

const middleware = getDefaultMiddleware(defaultMiddlewareConfig).concat(
  sagaMiddleware,
);

const devTools = Config.DEBUGGING === ENABLED;

export const store = configureStore({ reducer, middleware, devTools });
export const { dispatch } = store;
export const persistor = persistStore(store);
export type RootState = ReturnType<typeof store.getState>;

sagaMiddleware.run(rootSaga);
