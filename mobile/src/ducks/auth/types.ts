import { PayloadAction, CaseReducer } from '@reduxjs/toolkit';
import { AUTHORIZED, NOT_AUTHORIZED } from '../../utils/constants';
import { SignInForm } from '../../screens/auth/SignInScreen/types';
import { SignUpForm } from '../../screens/auth/SignUpScreen/types';

export type Status = typeof AUTHORIZED | typeof NOT_AUTHORIZED;
export type State = {
  status: Status;
};

export type SingInRequestAction = PayloadAction<SignInForm>;
export type SignInRequest = CaseReducer<State, SingInRequestAction>;
export type SignInFailure = CaseReducer<State, PayloadAction<any>>;
export type SignInSuccess = CaseReducer<State>;

export type SingUpRequestAction = PayloadAction<SignUpForm>;
export type SignUpRequest = CaseReducer<State, SingUpRequestAction>;
export type SignUpFailure = CaseReducer<State, PayloadAction<any>>;

export type SignOutSuccess = CaseReducer<State>;
export type SignOutFailure = CaseReducer<State, PayloadAction<any>>;
