import { createSlice } from '@reduxjs/toolkit';
import { NOT_AUTHORIZED, AUTHORIZED } from '../../utils/constants';
import {
  SignInFailure,
  SignInRequest,
  SignInSuccess,
  SignOutFailure,
  SignOutSuccess,
  SignUpFailure,
  SignUpRequest,
  State,
} from './types';

const name = 'auth';
const initialState: State = {
  status: NOT_AUTHORIZED,
};

// REDUCERS
const signInRequest: SignInRequest = () => undefined;
const signInSuccess: SignInSuccess = (state) => {
  state.status = AUTHORIZED;
};
const signInFailure: SignInFailure = () => undefined;
const signUpRequest: SignUpRequest = () => undefined;
const signUpSuccess = () => undefined;
const signUpFailure: SignUpFailure = () => undefined;
const signOutRequest = () => undefined;
const signOutSuccess: SignOutSuccess = (state) => {
  state.status = NOT_AUTHORIZED;
};
const signOutFailure: SignOutFailure = () => undefined;

const reducers = {
  signInFailure,
  signInRequest,
  signInSuccess,
  signOutFailure,
  signOutRequest,
  signOutSuccess,
  signUpFailure,
  signUpRequest,
  signUpSuccess,
};

export default createSlice({ name, initialState, reducers });
