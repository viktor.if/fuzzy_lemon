import { RootState } from '../../store';

export const getStatus = (state: RootState) => state.auth.status;
