import { createSlice } from '@reduxjs/toolkit';
import { actions as authActions } from '../auth';

const name = 'auth';
const initialState: boolean = false;
const reducers = {};

const extraReducers = {
  [authActions.signInRequest.type]: () => true,
  [authActions.signInSuccess.type]: () => false,
  [authActions.signInFailure.type]: () => false,
};

export default createSlice({ name, initialState, reducers, extraReducers });
