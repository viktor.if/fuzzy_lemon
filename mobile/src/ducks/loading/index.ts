import slice from './slice';
import * as selectors from './selectors';

export const { reducer } = slice;
export { selectors };
