import { createSlice, PayloadAction, CaseReducer } from '@reduxjs/toolkit';

import { State, Post, CreatePostRequest, CreatePostFailure, CreatePostSuccess, DeletePostRequest, DeletePostFailure, DeletePostSuccess, GetAllPostsRequest, GetAllPostsFailure, GetAllPostsSuccess } from './types';

const name = 'posts';

const initialState: State = {
  timeline: {
    data: [],
    loading: false,
    error: null,
  },
  item: {
    error: null,
    loading: false,
  }
};

const fetchPostsRequest: GetAllPostsRequest = (state) => {
  state.timeline.loading = true;
  state.timeline.error = null;
};

const fetchPostsFailure: GetAllPostsFailure = (state, { payload }) => {
  state.timeline.loading = false;
  state.timeline.error = payload.error;
};

const fetchPostsSuccess: GetAllPostsSuccess = (state, { payload }) => {
  state.timeline.loading = false;
  state.timeline.data = payload.data;
};

/** Create a post */
const createPostRequest: CreatePostRequest = (state) => {
  state.item.error = null;
  state.item.loading = true;
};

const createPostFailure: CreatePostFailure = (state, { payload }) => {
  state.item.error = payload.error;
  state.item.loading = false;
};

const createPostSuccess: CreatePostSuccess = (state, { payload }) => {
  state.item.loading = false;
  state.timeline.data.unshift(payload.data);
};

/** Delete a post */
const deletePostRequest: DeletePostRequest = (state) => {
  state.item.loading = true;
  state.item.error = null;
};

const deletePostFailure: DeletePostFailure = (state, { payload }) => {
  state.item.loading = false;
  state.item.error = payload.error;
};

const deletePostSuccess: DeletePostSuccess = (state, { payload }) => {
  state.item.loading = false;
  state.timeline.data = state.timeline.data.filter((item) => item.id !== payload.postId);
};

const reducers = {
  fetchPostsRequest,
  fetchPostsSuccess,
  fetchPostsFailure,
  createPostRequest,
  createPostFailure,
  createPostSuccess,
  deletePostRequest,
  deletePostFailure,
  deletePostSuccess,
};

export default createSlice({ name, initialState, reducers });
