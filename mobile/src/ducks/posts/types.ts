import { PayloadAction, CaseReducer } from '@reduxjs/toolkit';

export type Post = {
  id: string;
  createdBy: string;
  createdAt: string;
  changedAt: string;
  title: string;
  description: string;
  image: string;
};

export type PostTimeline = {
  data: Post[];
  loading: boolean;
  error: string | null;
};

export type State = {
  timeline: PostTimeline;
  item: {
    error: string | null;
    loading: boolean;
  }
};

/** Get all post */
export type GetAllPostsRequest = CaseReducer<State>;

export type GetAllPostsFailureAction = PayloadAction<{
  error: string;
}>;

export type GetAllPostsFailure = CaseReducer<
  State,
  GetAllPostsFailureAction
>;

export type GetAllPostsSuccessAction = PayloadAction<{
  data: Post[];
}>;

export type GetAllPostsSuccess = CaseReducer<
  State,
  GetAllPostsSuccessAction
>;

/** Create a post */
export type CreatePostRequestAction = PayloadAction<{
  title: string;
  description: string;
  image: string;
}>;

export type CreatePostRequest = CaseReducer<State, CreatePostRequestAction>;

export type CreatePostFailureAction = PayloadAction<{
  error: string;
}>;

export type CreatePostFailure = CaseReducer<
  State,
  CreatePostFailureAction
>;

export type CreatePostSuccessAction = PayloadAction<{
  data: Post;
}>;

export type CreatePostSuccess = CaseReducer<
  State,
  CreatePostSuccessAction
>;

/** Delete a post */
export type DeletePostRequestAction = PayloadAction<{
  postId: string;
}>;

export type DeletePostRequest = CaseReducer<State, DeletePostRequestAction>;

export type DeletePostFailureAction = PayloadAction<{
  error: string;
}>;

export type DeletePostFailure = CaseReducer<
  State,
  DeletePostFailureAction
>;

export type DeletePostSuccessAction = PayloadAction<{
  postId: string;
}>;

export type DeletePostSuccess = CaseReducer<
  State,
  DeletePostSuccessAction
>;
