import { RootState } from '../../store';

export const getAlertContent = (state: RootState) => state.alert;
