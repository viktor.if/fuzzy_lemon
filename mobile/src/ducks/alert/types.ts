import { PayloadAction, CaseReducer } from '@reduxjs/toolkit';

export type RequestError = {
  status: string | null;
  message: string;
};

export type OptionStyle = 'default' | 'cancel' | 'destructive';

type AlertOption = {
  text: string;
  onPress?: () => void;
  style?: OptionStyle;
};

export type Alert = {
  title: string;
  description: string;
  options: AlertOption[];
  requestError?: RequestError;
};

export type State = Alert | null;
export type SetAlert = CaseReducer<State, PayloadAction<Alert>>;
export type SetAlertWithError = CaseReducer<State, PayloadAction<any>>;
