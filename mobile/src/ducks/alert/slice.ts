import { createSlice } from '@reduxjs/toolkit';
import { actions as authActions } from '../auth';
import { actions as postActions } from '../posts';
import { State, SetAlert, SetAlertWithError } from './types';
import handleRequestError from '../../utils/handleRequestError';

const name = 'alert';
const initialState: State = null;

const setAlert: SetAlert = (_, { payload }) => payload;
const setAlertWithError: SetAlertWithError = (_, { payload }) =>
  handleRequestError(payload);
const unsetAlert = () => null;

const reducers = { setAlert, setAlertWithError, unsetAlert };
const extraReducers = {
  [authActions.signInFailure.type]: setAlertWithError,
  [authActions.signUpFailure.type]: setAlertWithError,
  [authActions.signOutFailure.type]: setAlertWithError,
  [postActions.fetchPostsFailure.type]: setAlertWithError,
};

export default createSlice({
  name,
  initialState: initialState as State,
  reducers,
  extraReducers,
});
