export const astronaut = '#2D4379';
export const backdrop = 'rgba(0,0,0,0.5)';
export const black = '#000000';
export const concrete = '#F3F3F3';
export const dodgerBlue = '#3366FF';
export const mineShaft = '#2F2F2F';
export const mercury = '#E5E5E5';
export const redOrange = '#FF4539';
export const scienceBlue = '#0047CC';
export const silver = '#CDCDCD';
export const silverChalice = '#B0B0B0';
export const transparent = 'rgba(0,0,0,0)';
export const white = '#FFFFFF';
export const wildBlueYonder = '#7B8BB2';
