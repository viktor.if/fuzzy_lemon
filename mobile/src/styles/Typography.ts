import { Platform } from 'react-native';
import { ANDROID } from '../utils/constants';

export const fontFamily = Platform.OS === ANDROID ? 'Roboto' : 'SF Pro';

export const fontWeight = {
  bold: '700',
  medium: '500',
  regular: '400',
  light: '300',
};
