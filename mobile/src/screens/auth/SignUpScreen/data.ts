import * as Yup from 'yup';
import { SignUpForm } from './types';
import i18n from '../../../configs/i18n';
import { AUTH } from '../../../utils/constants';

const emailSchema = Yup.string()
  .required(i18n.t(`${AUTH}:emailRequired`))
  .email(i18n.t(`${AUTH}:notValidEmail`));

const passwordSchema = Yup.string()
  .required(i18n.t(`${AUTH}:passwordRequired`))
  .min(8, i18n.t(`${AUTH}:needsMinQtyChars`))
  .matches(/[a-z]/, i18n.t(`${AUTH}:mustContainOneLowercaseLetter`))
  .matches(/[A-Z]/, i18n.t(`${AUTH}:mustContainOneUppercaseLetter`))
  .matches(/[\d]/, i18n.t(`${AUTH}:mustContainOneDigit`));

const confirmPasswordSchema = Yup.string()
  .oneOf([Yup.ref('password')], i18n.t(`${AUTH}:passwordsMustMatch`))
  .required(i18n.t(`${AUTH}:confirmRequired`));

export const validationSchema = Yup.object().shape({
  email: emailSchema,
  password: passwordSchema,
  confirmPassword: confirmPasswordSchema,
});

export const initialValues: SignUpForm = {
  email: '',
  password: '',
  confirmPassword: '',
};
