import React from 'react';
import { KeyboardAvoidingView, Platform, Keyboard } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Formik } from 'formik';
import { useTranslation } from 'react-i18next';

import Button from '../../../components/common/Button';
import TextInput from '../../../components/common/TextInput';
import styles from './styles';
import useAppDispatch from '../../../utils/hooks/useAppDispatch';
import useAppSelector from '../../../utils/hooks/useAppSelector';
import { ScreenTitle, ScreenSubtitle } from './styled';
import { SignUpForm } from './types';
import { actions as authActions } from '../../../ducks/auth';
import { selectors as loadingSelector } from '../../../ducks/loading';
import { validationSchema, initialValues } from './data';
import { iOS, HEIGHT, PADDING, AUTH } from '../../../utils/constants';

const SignUpScreen = (): JSX.Element => {
  const { t } = useTranslation(AUTH);
  const dispatch = useAppDispatch();
  const loading = useAppSelector(loadingSelector.getLoading);

  const onSubmit = (form: SignUpForm) => {
    Keyboard.dismiss();
    dispatch(authActions.signUpRequest(form));
  };

  return (
    <SafeAreaView style={styles.screen}>
      <ScreenTitle>{t('greetings')}</ScreenTitle>
      <ScreenSubtitle>{t('signUpCreateAccount')}</ScreenSubtitle>
      <KeyboardAvoidingView behavior={Platform.OS === iOS ? PADDING : HEIGHT}>
        <Formik {...{ initialValues, onSubmit, validationSchema }}>
          {({
            handleChange,
            handleSubmit,
            values,
            errors,
            touched,
            setFieldTouched,
          }) => (
            <>
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                error={errors.email}
                keyboardType="email-address"
                label={t('email')}
                onBlur={() => setFieldTouched('email')}
                onChangeText={handleChange('email')}
                placeholder={t('email')}
                touched={touched.email}
                value={values.email}
              />
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                error={errors.password}
                label={t('password')}
                onBlur={() => setFieldTouched('password')}
                onChangeText={handleChange('password')}
                placeholder={t('password')}
                touched={touched.password}
                value={values.password || ''}
              />
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                error={errors.confirmPassword}
                label={t('confirmPassword')}
                onBlur={() => setFieldTouched('confirmPassword')}
                onChangeText={handleChange('confirmPassword')}
                placeholder={t('confirmPassword')}
                touched={touched.confirmPassword}
                value={values.confirmPassword || ''}
              />
              <Button
                title={t('signUp')}
                onPress={handleSubmit}
                type="solid"
                loading={loading}
              />
            </>
          )}
        </Formik>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

export default SignUpScreen;
