import styled from 'styled-components/native';
import Text from '../../../components/common/Text';
import { Typography } from '../../../styles';

export const ScreenTitle = styled(Text)`
  font-size: 24px;
  font-family: ${Typography.fontFamily};
  font-weight: ${Typography.fontWeight.bold};
  margin-top: 32px;
`;

export const ScreenSubtitle = styled(Text)`
  font-size: 16px;
  margin-bottom: 32px;
`;
