import { StackNavigationProp } from '@react-navigation/stack';
import { CompositeNavigationProp } from '@react-navigation/native';
import { Pages } from '../../../navigators/Routes';
import {
  AuthStackParamList,
  RootStackParamList,
} from '../../../navigators/types';

export type SignUpScreenNavigationProp = CompositeNavigationProp<
  StackNavigationProp<AuthStackParamList, Pages.SignUpScreen>,
  StackNavigationProp<RootStackParamList>
>;

export type Props = {};

export type SignUpForm = {
  email: string;
  password: string;
  confirmPassword: string;
};
