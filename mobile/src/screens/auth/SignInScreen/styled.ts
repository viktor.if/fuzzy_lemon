import styled from 'styled-components/native';
import Text from '../../../components/common/Text';
import { Colors, Typography } from '../../../styles';

export const TextPressable = styled(Text)`
  color: ${Colors.dodgerBlue};
  font-family: ${Typography.fontFamily};
  font-weight: ${Typography.fontWeight.bold};
`;

export const TextBottom = styled(Text)`
  text-align: center;
  margin-vertical: 8px;
`;

export const ScreenTitle = styled(Text)`
  font-size: 24px;
  font-family: ${Typography.fontFamily};
  font-weight: ${Typography.fontWeight.bold};
  margin-top: 32px;
`;

export const ScreenSubtitle = styled(Text)`
  font-size: 16px;
  margin-bottom: 32px;
`;
