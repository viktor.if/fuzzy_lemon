import { StackNavigationProp } from '@react-navigation/stack';
import { CompositeNavigationProp } from '@react-navigation/native';
import { Pages } from '../../../navigators/Routes';
import {
  AuthStackParamList,
  RootStackParamList,
} from '../../../navigators/types';

export type SignInScreenNavigationProp = CompositeNavigationProp<
  StackNavigationProp<AuthStackParamList, Pages.SignInScreen>,
  StackNavigationProp<RootStackParamList>
>;

export type SignInForm = {
  email: string;
  password: string;
};
