import React from 'react';
import { KeyboardAvoidingView, Platform, Keyboard } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Formik } from 'formik';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';

import {
  TextPressable,
  TextBottom,
  ScreenTitle,
  ScreenSubtitle,
} from './styled';
import TextInput from '../../../components/common/TextInput';
import Button from '../../../components/common/Button';

import styles from './styles';
import useAppDispatch from '../../../utils/hooks/useAppDispatch';
import useAppSelector from '../../../utils/hooks/useAppSelector';
import { Pages } from '../../../navigators/Routes';
import { SignInForm, SignInScreenNavigationProp } from './types';
import { actions as authActions } from '../../../ducks/auth';
import { selectors as loadingSelector } from '../../../ducks/loading';
import { validationSchema, initialValues } from './data';
import { iOS, PADDING, HEIGHT, AUTH } from '../../../utils/constants';

const SignInScreen = (): JSX.Element => {
  const { t } = useTranslation(AUTH);
  const navigation = useNavigation<SignInScreenNavigationProp>();
  const dispatch = useAppDispatch();
  const loading = useAppSelector(loadingSelector.getLoading);

  const onSubmit = (form: SignInForm) => {
    Keyboard.dismiss();
    dispatch(authActions.signInRequest(form));
  };

  const handleSignupPressed = () => {
    navigation.navigate(Pages.SignUpScreen);
  };

  return (
    <SafeAreaView style={styles.screen}>
      <ScreenTitle>{t('welcomeBack')}</ScreenTitle>
      <ScreenSubtitle>{t('signInWithAccount')}</ScreenSubtitle>
      <KeyboardAvoidingView behavior={Platform.OS === iOS ? PADDING : HEIGHT}>
        <Formik {...{ initialValues, onSubmit, validationSchema }}>
          {({
            handleChange,
            handleSubmit,
            values,
            errors,
            touched,
            setFieldTouched,
          }) => (
            <>
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                error={errors.email}
                keyboardType="email-address"
                label={t('email')}
                onBlur={() => setFieldTouched('email')}
                onChangeText={handleChange('email')}
                placeholder={t('email')}
                textContentType="emailAddress"
                touched={touched.email}
                value={values.email}
                autoCompleteType="email"
              />
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                error={errors.password}
                label={t('password')}
                onBlur={() => setFieldTouched('password')}
                onChangeText={handleChange('password')}
                placeholder={t('password')}
                touched={touched.password}
                type="secure-text"
                value={values.password || ''}
              />
              <Button
                title={t('logIn')}
                onPress={handleSubmit}
                type="solid"
                loading={loading}
              />
            </>
          )}
        </Formik>
      </KeyboardAvoidingView>
      <TextBottom>
        {t('notHaveAccount?')}
        <TextPressable onPress={handleSignupPressed}>
          &ensp;
          {t('signUp')}
        </TextPressable>
      </TextBottom>
    </SafeAreaView>
  );
};

export default SignInScreen;
