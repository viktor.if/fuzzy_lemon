import * as Yup from 'yup';
import { SignInForm } from './types';
import i18n from '../../../configs/i18n';
import { AUTH } from '../../../utils/constants';

const emailSchema = Yup.string()
  .required(i18n.t(`${AUTH}:emailRequired`))
  .email(i18n.t(`${AUTH}:notValidEmail`));

const passwordSchema = Yup.string().required(
  i18n.t(`${AUTH}:passwordRequired`),
);

export const validationSchema = Yup.object().shape({
  email: emailSchema,
  password: passwordSchema,
});

export const initialValues: SignInForm = { email: '', password: '' };
