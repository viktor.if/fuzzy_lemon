import styled from 'styled-components/native';
import Text from '../../../components/common/Text';
import { Colors, Typography } from '../../../styles';

export const TopContainer = styled.TouchableOpacity`
  width: 100%;
`;

export const Container = styled.View`
  width: 100%;
`;

export const ImageWrapper = styled.TouchableOpacity`
  width: 100%;
  align-items: center;
  height: 200px;
  justify-content: center;
  border-radius: 8px;
  overflow: hidden;
`;
