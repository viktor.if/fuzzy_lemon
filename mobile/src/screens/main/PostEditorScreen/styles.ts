import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 16,
  },
  heading: {
    fontSize: 24,
  },
  input: {
    marginHorizontal: 16
  },
  topContainer: {
    flex: 1
  },
  image: {
    width: '100%',
    height: '100%'
  }
});
