import React, { useState } from 'react';
import { Image, Text } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useTranslation } from 'react-i18next';
import ImagePicker from 'react-native-image-crop-picker';
import { trackEvent } from 'appcenter-analytics';

import styles from './styles';
import TextInput from '../../../components/common/TextInput';
import { Container, ImageWrapper, TopContainer } from './styled'
import Button from '../../../components/common/Button';
import SvgIcon from '../../../components/common/SvgIcon';
import { Colors } from '../../../styles';
import useAppDispatch from '../../../utils/hooks/useAppDispatch';
import { actions as postActions } from '../../../ducks/posts';
import useAppSelector from '../../../utils/hooks/useAppSelector';

const PostEditorScreen = (): JSX.Element => {
  const { t } = useTranslation();
  const dispatch = useAppDispatch();

  const { loading } = useAppSelector((state) => state.posts.item);

  const [description, setDescription] = useState<string>('');
  const [image, setImage] = useState<string>('');

  const openCamera = async () => {
    try {
      const { path } = await ImagePicker.openCamera({
        width: 1080,
        height: 1080 * 0.6,
        mediaType: 'photo',
        cropping: true,
      });
      setImage(path);
    } catch {}
  }

  const uploadPost = () => {
    dispatch(postActions.createPostRequest({
      title: description,
      description,
      image,
    }));

    trackEvent('create_post', { title: description });
  }

  return (
    <SafeAreaView style={styles.container}>
      <Container>
        <TextInput
          value={description}
          onChangeText={(text) => setDescription(text)}
          label={t('common:description')}
          onBlur={() => {}}
          placeholder={t('common:description')}
        />
        <ImageWrapper  onPress={openCamera}>
          {image ? (
            <Image source={{ uri: image }} style={styles.image} />
          ) : (
            <SvgIcon name="image-plus" color={Colors.dodgerBlue} size={98} />
          )}
        </ImageWrapper>
      </Container>
      <Container>
        <Button
          title={t('common:save')}
          type="solid"
          disabled={!description || !image}
          onPress={uploadPost}
          loading={loading}
        />
      </Container>
    </SafeAreaView>
  );
};

export default PostEditorScreen;
