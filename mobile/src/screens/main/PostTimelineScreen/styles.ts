import { StyleSheet } from 'react-native';
import { Colors, Metrics, Typography } from '../../../styles';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.concrete,
  },
  heading: {
    fontSize: 24,
  },
  flatlist: {
    flexGrow: 1,
    width: Metrics.screenWidth,
  }
});
