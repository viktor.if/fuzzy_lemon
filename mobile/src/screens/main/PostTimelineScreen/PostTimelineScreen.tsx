import React, { useEffect } from 'react';
import { FlatList, ActivityIndicator } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useTranslation } from 'react-i18next';

import styles from './styles';
import useAppDispatch from '../../../utils/hooks/useAppDispatch';
import useAppSelector from '../../../utils/hooks/useAppSelector';
import { actions as postActions } from '../../../ducks/posts';
import { actions as alertActions } from '../../../ducks/alert';
import PostListItem from '../../../components/common/PostListItem';
import { NoImagesText } from './styled';
import { Colors } from '../../../styles'

const PostTimelineScreen = (): JSX.Element => {
  const [t] = useTranslation();
  const dispatch = useAppDispatch();
  const { data: posts, loading } = useAppSelector((state) => state.posts.timeline);

  useEffect(() => {
    dispatch(postActions.fetchPostsRequest());
  }, [postActions]);

  const onDeletePost = (postId: string) => {
    dispatch(alertActions.setAlert({
      title: t('common:deleteImage'),
      description: t('common:areYouSureQuestion'),
      options: [
        {
          text: t('common:delete'),
          onPress: () => dispatch(postActions.deletePostRequest({ postId })),
          style: 'destructive',
        },
        { text: t('common:cancel') },
      ]
    }))
  }

  return (
    <SafeAreaView style={styles.container}>
      { loading ? (
        <ActivityIndicator color={Colors.dodgerBlue} size="large" />
      ) : !posts.length ? (
        <NoImagesText>{t('common:noImagesYet')}</NoImagesText>
      ) : (
        <FlatList style={styles.flatlist} keyExtractor={(item) => item.id} data={posts} renderItem={({ item }) => (
          <PostListItem
            key={item.id}
            imageUrl={item.image}
            title={item.description}
            date={item.createdAt}
            onLongPress={() => onDeletePost(item.id)}
          />
        )} />
      )}
    </SafeAreaView>
  );
};

export default PostTimelineScreen;
