import styled from 'styled-components/native';
import Text from '../../../components/common/Text';
import { Colors, Typography } from '../../../styles';

export const NoImagesText = styled(Text)`
  color: ${Colors.dodgerBlue};
  font-family: ${Typography.fontFamily};
  font-weight: ${Typography.fontWeight.bold};
`;
