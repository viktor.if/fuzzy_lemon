import { StackNavigationProp } from '@react-navigation/stack';
import { CompositeNavigationProp } from '@react-navigation/native';
import { Pages } from '../../../navigators/Routes';
import {
  BottomTabParamList,
  RootStackParamList,
} from '../../../navigators/types';

export type SettingsScreenNavigationProp = CompositeNavigationProp<
  StackNavigationProp<BottomTabParamList, Pages.SettingsScreen>,
  StackNavigationProp<RootStackParamList>
>;
