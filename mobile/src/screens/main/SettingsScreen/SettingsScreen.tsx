import React from 'react';
import { Text } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useTranslation } from 'react-i18next';

import Button from '../../../components/common/Button';
import styles from './styles';
import useAppDispatch from '../../../utils/hooks/useAppDispatch';
import { actions as alertActions } from '../../../ducks/alert';
import { actions as authActions } from '../../../ducks/auth';
import { AUTH } from '../../../utils/constants';

const SettingsScreen = (): JSX.Element => {
  const dispatch = useAppDispatch();
  const { t } = useTranslation();

  const handleSignoutPressed = () => {
    dispatch(
      alertActions.setAlert({
        title: t(`${AUTH}:logout`),
        description: t('areYouSure?'),
        options: [
          {
            text: t(`${AUTH}:yesLogOut`),
            onPress: () => dispatch(authActions.signOutRequest()),
          },
          {
            text: t('cancel'),
            style: 'cancel',
          },
        ],
      }),
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <Button
        title={t(`${AUTH}:logOut`)}
        onPress={handleSignoutPressed}
        type="solid"
      />
    </SafeAreaView>
  );
};

export default SettingsScreen;
