import _ from 'lodash';
import Config from 'react-native-config';

/** Manage a request */
export default class RequestHelper {
  static fetch = async (url: string, options: RequestInit): Promise<any> => {
    const controller = new AbortController();
    const timeoutId = setTimeout(() => controller.abort(), 4000);

    _.set(options, 'signal', controller.signal);

    try {
      const response = await fetch(`${Config.BASE_URL}/${url}`, options);

      clearTimeout(timeoutId);

      if (__DEV__) {
        // eslint-disable-next-line no-console
        console.log('=== Next request ===');
        // eslint-disable-next-line no-console
        console.log(`> ${options.method} ${url}`);
        // eslint-disable-next-line no-console
        console.log(`< Status ${response.status}`);
      }
      switch (response.status) {
        case 204: {
          return {};
        }
        case 400:
        case 401:
        case 403:
        case 404:
        case 409: {
          const keys = ['error', 'message', 'path'];
          const error = await response.json();
          const [msg] = Object.keys(error).filter((el) => keys.includes(el));

          // eslint-disable-next-line no-console
          if (__DEV__ && !_.isEmpty(msg)) console.log(`< Msg: ${error[msg]}`);
console.log({ response})
          return await Promise.reject({
            status: response.status,
            message: msg ? error[msg] : 'User error',
          });
        }
        case 500:
        case 502: {
          return await Promise.reject({
            status: response.status,
            message: 'Internal server error',
          });
        }
        case 503: {
          return await Promise.reject({
            status: response.status,
            message: 'The server is being updated...',
          });
        }
        default: {
          // Success
          return await response.json();
        }
      }
    } catch (error: any) {
      return Promise.reject({
        message: error?.message ?? 'Oops, something went wrong...',
      });
    }
  };
}