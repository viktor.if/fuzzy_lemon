import { AxiosError } from 'axios';
import _ from 'lodash';
import { RequestError, Alert } from '../ducks/alert/types';
import i18n from '../configs/i18n';
import { ERROR } from './constants';

type CognitoError = {
  code: string;
  message: string;
};

const isAxiosError = (error: any): error is AxiosError =>
  !_.isUndefined(error.isAxiosError);

const isCognitoError = (error: any): error is CognitoError =>
  !_.isUndefined(error.code) && !_.isUndefined(error.message);

const normalizeError = (error: any): RequestError => {
  if (isAxiosError(error)) {
    return {
      message: error.response?.data.message,
      status: _.toString(error.response?.status),
    };
  }

  if (isCognitoError(error)) {
    return { status: error.code, message: error.message };
  }

  const message = _.isString(error.message)
    ? error.message
    : i18n.t(`${ERROR}:somethingWentWrong`);

  return { status: null, message };
};

/**
 * Handle request error by alert reducers
 */

export default (error: any): Alert => {
  const errorNormalized = normalizeError(error);

  switch (errorNormalized.status) {
    case 'NotAuthorizedException': {
      return {
        title: i18n.t('error'),
        description: i18n.t(`${ERROR}:emailOrPasswordIncorrect`),
        options: [{ text: i18n.t('okay') }],
        requestError: errorNormalized,
      };
    }
    case 'UsernameExistsException': {
      return {
        title: i18n.t('error'),
        description: i18n.t(`${ERROR}:emailAlreadyTaken`),
        options: [{ text: i18n.t('okay') }],
        requestError: errorNormalized,
      };
    }
    case 'UserNotConfirmedException': {
      return {
        title: i18n.t('error'),
        description: i18n.t(`${ERROR}:userNotConfirmed`),
        options: [{ text: i18n.t('okay') }],
        requestError: errorNormalized,
      };
    }
    default: {
      return {
        title: i18n.t('error'),
        description: errorNormalized.message,
        options: [{ text: i18n.t('okay') }],
        requestError: errorNormalized,
      };
    }
  }
};
