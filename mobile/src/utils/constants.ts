export const PRODUCTION = 'production';
export const AUTHORIZED = 'AUTHORIZED';
export const NOT_AUTHORIZED = 'NOT_AUTHORIZED';
export const UK = 'uk';
export const EN = 'en';
export const ANDROID = 'android';
export const IOS = 'ios';
export const HEIGHT = 'height';
export const PADDING = 'padding';
export const COMMON = 'common';
export const AUTH = 'auth';
export const ERROR = 'error';
export const ENABLED = 'enabled';
