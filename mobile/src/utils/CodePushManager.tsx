import React from 'react';
import codePush from 'react-native-code-push';
import Config from 'react-native-config';
import { Platform } from 'react-native';
import { ANDROID } from './constants';

class CodePushManager extends React.Component {
  componentDidMount() {
    codePush.notifyAppReady().then(() => {
      if (Platform.OS === ANDROID) {
        codePush.sync({
          deploymentKey: Config.CODEPUSH_ANDROID_STAGING_KEY,
        });
      }
    });
  }

  render() {
    return null;
  }
}

export default codePush({ checkFrequency: codePush.CheckFrequency.MANUAL })(
  CodePushManager,
);
