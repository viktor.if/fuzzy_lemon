import {
  CommonActions,
  NavigationContainerRef,
} from '@react-navigation/native';
import { Pages } from '../navigators/Routes';

/**
 * Common navagation actions
 */

type Navigator = NavigationContainerRef | null;

let navigator: Navigator = null;

export const setNavigator = (navigatorRef: NavigationContainerRef | null) => {
  navigator = navigatorRef;
};

export const navigate = (name: Pages, params?: object, key?: string) => {
  navigator?.dispatch(
    CommonActions.navigate({
      name,
      params,
      key,
    }),
  );
};

export const goBack = (source?: string, target?: string) => {
  navigator?.dispatch({
    ...CommonActions.goBack(),
    source,
    target,
  });
};
