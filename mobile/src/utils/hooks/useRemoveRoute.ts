import { useEffect } from 'react';
import { CommonActions, StackNavigationState } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { Pages } from '../../navigators/Routes';
import { RootStackParamList } from '../../navigators/types';

type Route = Pages.AuthStack | Pages.MainStack;

/**
 * Removes a route from the navigation state
 */

export default (
  navigation: StackNavigationProp<RootStackParamList>,
  route: Route,
) => {
  useEffect(() => {
    const resetState = (state: StackNavigationState<RootStackParamList>) => {
      const routesNext = state.routes.filter((r: any) => r.name !== route);

      return CommonActions.reset({
        ...state,
        routes: routesNext,
        index: routesNext.length - 1,
      });
    };

    navigation.dispatch(resetState);
  }, []);
};
