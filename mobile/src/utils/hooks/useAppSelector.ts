import { useSelector, TypedUseSelectorHook } from 'react-redux';
import { RootState } from '../../store';

/**
 * Typed useSelector hook
 */

const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
export default useAppSelector;
