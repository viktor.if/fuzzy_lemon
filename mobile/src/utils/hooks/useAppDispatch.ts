import { useDispatch } from 'react-redux';
import { store } from '../../store';

/**
 * Typed useDispatch hook
 */

type AppDispatch = typeof store.dispatch;

export default () => useDispatch<AppDispatch>();
