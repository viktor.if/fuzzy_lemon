import { all } from 'redux-saga/effects';
import authSaga from './authSaga';
import postSaga from './postSaga';

export default function* () {
  yield all([authSaga(), postSaga()]);
}
