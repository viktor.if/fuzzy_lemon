import { takeLatest, put, call } from 'redux-saga/effects';
import { Auth } from 'aws-amplify';
import { CognitoUserSession } from 'amazon-cognito-identity-js';

import * as navigationActions from '../utils/navigationActions';
import { Pages } from '../navigators/Routes';
import { SingInRequestAction, SingUpRequestAction } from '../ducks/auth/types';
import { actions as authActions } from '../ducks/auth';
import { actions as alertActions } from '../ducks/alert';

function* signInSaga(action: SingInRequestAction) {
  const { email, password } = action.payload;

  try {
    yield call([Auth, 'signIn'], email, password);
    yield put(authActions.signInSuccess());
    navigationActions.navigate(Pages.MainStack);
  } catch (error) {
    yield put(authActions.signInFailure(error));
  }
}

function* signUpSaga(action: SingUpRequestAction) {
  const { email, password } = action.payload;

  try {
    yield call([Auth, 'signUp'], email, password);
    yield put(authActions.signUpSuccess());
    navigationActions.goBack();

    // TODO: translate
    yield put(alertActions.setAlert({
      title: 'Success',
      description: 'You have been registered. Please check your email to finish registration',
      options: [{
        text: 'Ok'
      }],
    }))
  } catch (error) {
    yield put(authActions.signUpFailure(error));
  }
}

function* signOutSaga() {
  try {
    yield put(authActions.signOutSuccess());
    yield call(Auth.signOut);
    navigationActions.navigate(Pages.AuthStack);
  } catch (error) {
    yield put(authActions.signOutFailure(error));
  }
}

export function* getIdToken() {
  let idToken;

  try {
    const session: CognitoUserSession = yield call([Auth, 'currentSession']);
    idToken = session.getIdToken();
  } catch (error) {
    yield alertActions.setAlertWithError(error);
  }

  return idToken;
}

export default function* () {
  yield takeLatest(authActions.signInRequest, signInSaga);
  yield takeLatest(authActions.signUpRequest, signUpSaga);
  yield takeLatest(authActions.signOutRequest, signOutSaga);
}
