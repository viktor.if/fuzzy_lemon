import { CognitoIdToken } from 'amazon-cognito-identity-js';
import { takeLatest, put, call } from 'redux-saga/effects';

import { getPosts, createPost, deletePost } from '../api/posts';
import { actions as postActions } from '../ducks/posts';
import { goBack, navigate } from '../utils/navigationActions';
import { getIdToken } from './authSaga';

type Await<T> = T extends PromiseLike<infer U> ? U : T;

function* fetchPostsSaga(): any {
  try {
    const idToken = yield getIdToken();
    const response = yield call(getPosts, { idToken });

    const { data } = response;

    yield put(postActions.fetchPostsSuccess({ data }));
  } catch (error) {
    yield put(postActions.fetchPostsFailure({ error: 'Error occurred'}));
  }
}

function* uploadPostSaga({
  payload: { title, description, image },
}: ReturnType<typeof postActions.createPostRequest>): any {
  try {
    const idToken: CognitoIdToken = yield getIdToken();

    const response: Await<ReturnType<typeof createPost>> = yield call<typeof createPost>(
      createPost,
      { idToken, title, description, image }
    );

    // const { data } = response;
    console.log({ response })

    yield put(postActions.createPostSuccess({ data: response }));
    goBack()
  } catch (error) {
    console.log({ error })
    yield put(postActions.createPostFailure({ error: 'Error occurred'}));
  }
}

function* deletePostSaga({
  payload: { postId },
}: ReturnType<typeof postActions.deletePostRequest>): any {
  try {
    const idToken: CognitoIdToken = yield getIdToken();

    yield call<typeof deletePost>(
      deletePost,
      { idToken, postId }
    );

    yield put(postActions.deletePostSuccess({ postId }));
    goBack()
  } catch (error) {
    console.log({ error })
    yield put(postActions.deletePostFailure({ error: 'Error occurred'}));
  }
}

export default function* () {
  yield takeLatest(postActions.fetchPostsRequest, fetchPostsSaga);
  yield takeLatest(postActions.createPostRequest, uploadPostSaga);
  yield takeLatest(postActions.deletePostRequest, deletePostSaga)
}
