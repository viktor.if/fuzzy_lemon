import i18n from 'i18next';
import { NativeModules, Platform } from 'react-native';
import { initReactI18next } from 'react-i18next';

import {
  IOS,
  EN,
  UK,
  COMMON,
  AUTH,
  ERROR,
} from '../utils/constants';

const locale =
  Platform.OS === IOS
    ? NativeModules.SettingsManager.settings.AppleLocale
    : NativeModules.I18nManager.localeIdentifier;

i18n.use(initReactI18next).init({
  resources: {},
  ns: [COMMON],
  defaultNS: COMMON,
  fallbackNS: COMMON,
  fallbackLng: EN,
  lng: locale?.substring(0, 2),
  debug: false,
  interpolation: {
    escapeValue: false,
  },
});

// ADD ENGLISH LOCALE
i18n.addResources(EN, COMMON, require('../locales/en/common.json'));
i18n.addResources(EN, AUTH, require('../locales/en/auth.json'));
i18n.addResources(EN, ERROR, require('../locales/en/error.json'));

// ADD UKRAINIAN LOCALE
i18n.addResources(UK, COMMON, require('../locales/uk/common.json'));
i18n.addResources(UK, AUTH, require('../locales/uk/auth.json'));
i18n.addResources(UK, ERROR, require('../locales/uk/error.json'));

export default i18n;
