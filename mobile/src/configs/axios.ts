import Config from 'react-native-config';
import axios from 'axios';

/**
 * Axios config defaults
 */

axios.defaults.baseURL = Config.BASE_URL;
axios.defaults.headers.common['Content-Type'] = 'application/json';
axios.defaults.timeout = 10000;
