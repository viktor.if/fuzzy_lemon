const awsmobile = {
  aws_project_region: 'us-east-1',
  aws_cognito_region: 'us-east-1',
  aws_user_pools_id: 'us-east-1_4MUxALp5t',
  aws_user_pools_web_client_id: 'a9kucaa5t2i67i8s32l88l17t',
  oauth: {
    domain: 'fjord-dev.auth.us-east-1.amazoncognito.com',
  },
};

export default awsmobile;
