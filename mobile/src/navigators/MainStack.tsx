import React from 'react';
import {
  createStackNavigator,
  StackNavigationProp,
} from '@react-navigation/stack';
import { useNavigation } from '@react-navigation/native';
import BottomTab from './BottomTab';
import PostEditorScreen from '../screens/main/PostEditorScreen';
import { Pages } from './Routes';
import { MainStackParamList, RootStackParamList } from './types';
import ScreenHeader from '../components/common/ScreenHeader';
import useRemoveRoute from '../utils/hooks/useRemoveRoute';
import i18n from '../configs/i18n';

const Stack = createStackNavigator<MainStackParamList>();

const MainStack = () => {
  const navigation = useNavigation<StackNavigationProp<RootStackParamList>>();
  useRemoveRoute(navigation, Pages.AuthStack);

  return (
    <Stack.Navigator initialRouteName={Pages.BottomTab}>
      <Stack.Screen
        name={Pages.BottomTab}
        component={BottomTab}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name={Pages.PostEditorScreen}
        component={PostEditorScreen}
        options={{
          header: () => (
            <ScreenHeader
              leftButtonIcon="chevron-left"
              onPressLeftButton={() => navigation.goBack()}
              title={i18n.t('common:newImage')}
            />
          ),
        }}
      />
    </Stack.Navigator>
  );
};

export default MainStack;
