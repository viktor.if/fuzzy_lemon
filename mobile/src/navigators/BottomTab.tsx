import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import TabBar from '../components/navigation/TabBar';
import SettingsScreen from '../screens/main/SettingsScreen';
import PostTimelineScreen from '../screens/main/PostTimelineScreen';
import { Pages } from './Routes';
import { BottomTabParamList } from './types';

const Tab = createBottomTabNavigator<BottomTabParamList>();

const BottomTab = (): JSX.Element => (
  <Tab.Navigator initialRouteName={Pages.PostTimelineScreen} tabBar={TabBar}>
    <Tab.Screen
      name={Pages.PostTimelineScreen}
      component={PostTimelineScreen}
    />
    <Tab.Screen name={Pages.SettingsScreen} component={SettingsScreen} />
  </Tab.Navigator>
);

export default BottomTab;
