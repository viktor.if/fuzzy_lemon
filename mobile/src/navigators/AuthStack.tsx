import React from 'react';
import {
  createStackNavigator,
  StackNavigationProp,
} from '@react-navigation/stack';
import { useNavigation } from '@react-navigation/native';
import SignInScreen from '../screens/auth/SignInScreen';
import SignUpScreen from '../screens/auth/SignUpScreen';
import { Pages } from './Routes';
import { AuthStackParamList, RootStackParamList } from './types';
import ScreenHeader from '../components/common/ScreenHeader';
import useRemoveRoute from '../utils/hooks/useRemoveRoute';

const Stack = createStackNavigator<AuthStackParamList>();

const AuthStack = () => {
  const navigation = useNavigation<StackNavigationProp<RootStackParamList>>();
  useRemoveRoute(navigation, Pages.MainStack);

  return (
    <Stack.Navigator>
      <Stack.Screen
        name={Pages.SignInScreen}
        component={SignInScreen}
        options={{ header: () => <ScreenHeader /> }}
      />
      <Stack.Screen
        name={Pages.SignUpScreen}
        component={SignUpScreen}
        options={{
          header: () => (
            <ScreenHeader
              leftButtonIcon="chevron-left"
              onPressLeftButton={() => navigation.goBack()}
            />
          ),
        }}
      />
    </Stack.Navigator>
  );
};

export default AuthStack;
