export enum Pages {
  AuthStack = 'AuthStack',
  BottomTab = 'BottomTab',
  MainStack = 'MainStack',
  PostEditorScreen = 'PostEditorScreen',
  PostTimelineScreen = 'PostTimelineScreen',
  SettingsScreen = 'SettingsScreen',
  SignInScreen = 'SignInScreen',
  SignUpScreen = 'SignUpScreen',
}
