import { Pages } from './Routes';

export type RootStackParamList = {
  [Pages.AuthStack]: undefined;
  [Pages.MainStack]: undefined;
};

export type AuthStackParamList = {
  [Pages.SignInScreen]: undefined;
  [Pages.SignUpScreen]: undefined;
};

export type BottomTabParamList = {
  [Pages.PostTimelineScreen]: undefined;
  [Pages.SettingsScreen]: undefined;
};

export type MainStackParamList = {
  [Pages.BottomTab]: undefined;
  [Pages.PostEditorScreen]: undefined;
};
