import 'react-native-gesture-handler';
import React, { useRef } from 'react';
import { StatusBar } from 'react-native';
import {
  NavigationContainer,
  NavigationContainerRef,
} from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import * as navigationActions from '../utils/navigationActions';
import AuthStack from './AuthStack';
import MainStack from './MainStack';
import useAppSelector from '../utils/hooks/useAppSelector';
import { AUTHORIZED } from '../utils/constants';
import { AppTheme } from './AppTheme';
import { Colors } from '../styles';
import { Pages } from './Routes';
import { RootStackParamList } from './types';
import { selectors as authSelectors } from '../ducks/auth';

const Stack = createStackNavigator<RootStackParamList>();

const RootStack = () => {
  const navigationRef = useRef<NavigationContainerRef>(null);
  const authStatus = useAppSelector(authSelectors.getStatus);
  const isUserAuthorized = authStatus === AUTHORIZED;
  const initialRouteName = isUserAuthorized ? Pages.MainStack : Pages.AuthStack;

  const handleNavigationReady = () => {
    navigationActions.setNavigator(navigationRef.current);
  };

  return (
    <SafeAreaProvider>
      <StatusBar barStyle="dark-content" backgroundColor={Colors.white} />
      <NavigationContainer
        theme={AppTheme}
        ref={navigationRef}
        onReady={handleNavigationReady}
      >
        <Stack.Navigator mode="modal" {...{ initialRouteName }}>
          <Stack.Screen
            name={Pages.AuthStack}
            component={AuthStack}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name={Pages.MainStack}
            component={MainStack}
            options={{ headerShown: false }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </SafeAreaProvider>
  );
};

export default RootStack;
