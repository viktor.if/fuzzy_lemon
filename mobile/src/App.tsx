import React from 'react';
import { LogBox } from 'react-native';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import Amplify from 'aws-amplify';

import './configs/i18n';
import './configs/axios';
import GlobalAlert from './components/common/GlobalAlert';
import LoadingOverlay from './components/common/LoadingOverlay';
import RootNavigator from './navigators/RootStack';
import CodePushManager from './utils/CodePushManager';
import awsconfig from './configs/aws-exports';
import logsShouldBeIgnored from './utils/logsShouldBeIgnored';
import { store, persistor } from './store';

LogBox.ignoreLogs(logsShouldBeIgnored);
Amplify.configure(awsconfig);

const App = (): JSX.Element => (
  <Provider {...{ store }}>
    <PersistGate loading={null} persistor={persistor}>
      <RootNavigator />
      <CodePushManager />
      <LoadingOverlay />
      <GlobalAlert />
    </PersistGate>
  </Provider>
);

export default App;
