import styled from 'styled-components/native';
import { Colors, Typography } from '../../../styles';

export default styled.Text`
  color: ${Colors.mineShaft};
  font-family: ${Typography.fontFamily};
  font-weight: ${Typography.fontWeight.regular};
  font-size: 16px;
`;
