import React from 'react';
import { Image } from 'react-native';
import moment from 'moment';

import { Container, TextContainer, DescriptionText, DateText } from './styled';
import { PostListItemProps } from './types';
import styles from './styles';

export default ({ imageUrl, title, date, onLongPress }: PostListItemProps) => {

  console.log({ imageUrl})
  return (
    <Container {...{ onLongPress }} delayLongPress={1000}>
      <Image source={{ uri: imageUrl }} style={styles.image} resizeMode='cover' />
      <TextContainer>
        <DescriptionText numberOfLines={2}>{title}</DescriptionText>
        <DateText>{moment(date).format('DD/MM/YYYY')}</DateText>
      </TextContainer>
    </Container>
  )
};