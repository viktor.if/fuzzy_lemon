import styled from 'styled-components/native';
import Text from '../Text';
import { Colors, Typography } from '../../../styles';

export const Container = styled.TouchableOpacity`
  margin-left: 16px;
  margin-right: 16px;
  margin-top: 8px;
  margin-bottom: 8px;
  border-radius: 8px;
  overflow: hidden;
`;

export const TextContainer = styled.View`
  position: absolute;
  bottom: 0px;
  left: 0px;
  right: 0px
  flex-direction: row;
  flex: 1;
  background-color: rgba(0,0,0,0.4);
  align-items: flex-end;
`;

export const DescriptionText = styled.Text`
  font-size: 14px;
  flex-direction: row;
  flex: 1;
  color: #fff
  margin-left: 16px;
  margin-top: 6px;
  margin-bottom: 6px;
`

export const DateText = styled.Text`
  font-size: 14px;
  color: #fff
  margin-left: 24px;
  margin-top: 6px;
  margin-bottom: 6px;
  margin-right: 16px;
`

