import { GestureResponderEvent } from 'react-native';

export type PostListItemProps = {
  imageUrl: string;
  title: string;
  date: string;
  onLongPress: ((event: GestureResponderEvent) => void);
}