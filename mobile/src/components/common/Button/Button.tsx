import React from 'react';
import { ActivityIndicator } from 'react-native';
import { ThemeProvider } from 'styled-components';
import { Container, Title, Themes } from './styled';
import { Props } from './types';

const Button = ({
  disabled = false,
  loading = false,
  onPress,
  title,
  type,
}: Props): JSX.Element => {
  const isDisabled = disabled || loading;
  const theme = Themes[type];
  const activityColor = theme.textColor;

  return (
    <ThemeProvider {...{ theme }}>
      <Container {...{ onPress }} disabled={isDisabled} testID="button" activeOpacity={0.6}>
        {loading ? (
          <ActivityIndicator size="small" color={activityColor} testID='buttonActivity' />
        ) : (
          <Title testID="buttonText">{title}</Title>
        )}
      </Container>
    </ThemeProvider>
  );
};

export default Button;
