import { GestureResponderEvent } from 'react-native';

type ButtonType = 'solid' | 'clear' | 'outline';

export type Props = {
  disabled?: boolean;
  loading?: boolean;
  onPress: (event: GestureResponderEvent) => void;
  title: string;
  type: ButtonType;
};

export type Theme = {
  backgroundColor: string;
  textColor: string;
  borderColor: string;
  borderWidth: number;
};

export type ThemeProp = {
  theme: Theme;
};
