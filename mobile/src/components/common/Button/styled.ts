import { PixelRatio } from 'react-native';
import styled from 'styled-components/native';

import Text from '../Text';
import { Colors } from '../../../styles';
import { Theme, ThemeProp } from './types';

const SolidTheme: Theme = {
  backgroundColor: Colors.dodgerBlue,
  borderColor: Colors.dodgerBlue,
  borderWidth: 0,
  textColor: Colors.white,
};

const OutlineTheme: Theme = {
  backgroundColor: Colors.transparent,
  borderColor: Colors.dodgerBlue,
  borderWidth: 2,
  textColor: Colors.dodgerBlue,
};

const ClearTheme: Theme = {
  backgroundColor: Colors.transparent,
  borderColor: Colors.transparent,
  borderWidth: 0,
  textColor: Colors.dodgerBlue,
};

export const Themes = {
  clear: ClearTheme,
  outline: OutlineTheme,
  solid: SolidTheme,
};

export const Container = styled.TouchableOpacity<ThemeProp>`
  align-items: center;
  background-color: ${(props) => props.theme.backgroundColor};
  border-color: ${(props) => props.theme.borderColor};
  border-radius: ${PixelRatio.roundToNearestPixel(12)}px;
  border-width: ${(props) =>
    PixelRatio.roundToNearestPixel(props.theme.borderWidth)}px;
  height: ${PixelRatio.roundToNearestPixel(60)}px;
  justify-content: center;
  margin-vertical: 16px;
  width: 100%;
`;

export const Title = styled(Text)<ThemeProp>`
  color: ${(props) => props.theme.textColor ?? '#FFFFFF'};
  text-transform: uppercase;
`;

Container.defaultProps = {
  theme: SolidTheme,
};
