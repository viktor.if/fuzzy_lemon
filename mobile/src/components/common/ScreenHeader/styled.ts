import styled from 'styled-components/native';
import Text from '../Text';
import { Typography } from '../../../styles';
import { TitleContainerProps } from './types';

export const Container = styled.View`
  height: 60px;
  flex-direction: row;
  padding-right: 24px;
`;

export const ContentLeft = styled.View`
  flex-direction: row;
  flex: 1;
  align-items: center;
`;

export const ButtonLeft = styled.Pressable`
  padding-left: 12px;
  height: 100%;
  justify-content: center;
`;

export const TitleContainer = styled.View<TitleContainerProps>`
  padding-left: ${({ leftButtonShown }) => (leftButtonShown ? 4 : 24)}px;
  flex-direction: row;
  flex: 1;
`;

export const Title = styled(Text)`
  font-size: 24px;
  font-family: ${Typography.fontFamily};
  font-weight: ${Typography.fontWeight.medium};
`;
