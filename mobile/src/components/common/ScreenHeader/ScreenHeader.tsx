import React from 'react';
import _ from 'lodash';
import {
  Container,
  ContentLeft,
  TitleContainer,
  Title,
  ButtonLeft,
} from './styled';
import { Props } from './types';
import SvgIcon from '../SvgIcon';

const ScreenHeader = ({
  title,
  leftButtonIcon,
  onPressLeftButton,
}: Props): JSX.Element => {
  const showTitle: boolean = _.isString(title);
  const showLeftButton: boolean =
    _.isString(leftButtonIcon) && _.isFunction(onPressLeftButton);

  return (
    <Container>
      <ContentLeft>
        {showLeftButton && (
          <ButtonLeft onPress={onPressLeftButton}>
            <SvgIcon name="chevron-left" size={36} />
          </ButtonLeft>
        )}
        {showTitle && (
          <TitleContainer leftButtonShown={showLeftButton}>
            <Title numberOfLines={1}>{title}</Title>
          </TitleContainer>
        )}
      </ContentLeft>
    </Container>
  );
};

export default ScreenHeader;
