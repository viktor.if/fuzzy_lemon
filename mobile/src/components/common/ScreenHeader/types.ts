import { IconName } from '../SvgIcon/types';

export type Props = {
  title?: string;
  leftButtonIcon?: IconName;
  onPressLeftButton?: () => void;
};

export type TitleContainerProps = {
  leftButtonShown: boolean;
};
