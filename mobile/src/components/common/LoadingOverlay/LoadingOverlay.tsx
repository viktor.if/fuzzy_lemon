import React from 'react';
import useAppSelector from '../../../utils/hooks/useAppSelector';
import { Container } from './styled';
import { selectors as loadingSelectors } from '../../../ducks/loading';

const LoadingOverlay = (): JSX.Element | null => {
  const loading = useAppSelector(loadingSelectors.getLoading);

  return loading ? <Container /> : null;
};

export default LoadingOverlay;
