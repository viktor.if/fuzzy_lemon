import React from 'react';
import _ from 'lodash';

import useAppSelector from '../../../utils/hooks/useAppSelector';
import useAppDispatch from '../../../utils/hooks/useAppDispatch';
import { actions as alertActions } from '../../../ducks/alert';

import {
  Container,
  Content,
  Description,
  Option,
  OptionText,
  Title,
} from './styled';

const GlobalAlert = (): JSX.Element | null => {
  const alertContent = useAppSelector((state) => state.alert);
  const dispatch = useAppDispatch();

  console.log({ alertContent})

  if (_.isNull(alertContent)) return null;
  
  const { title, description, options } = alertContent;

  const handleOptionPressed = (applyOption?: () => void) => {
    dispatch(alertActions.unsetAlert());
    if (!_.isUndefined(applyOption)) applyOption();
  };

  return (
    <Container testID='_globalAlert'>
      <Content>
        <Title testID='globalAlert_title'>{title}</Title>
        <Description numberOfLines={4} testID='globalAlert_description'>{description}</Description>
        {options.map((option, index) => (
          <Option
            key={_.toString(index)}
            onPress={() => handleOptionPressed(option.onPress)}
            testID={`globalAlert_option_${index}`}
          >
            <OptionText type={option.style} testID={`globalAlert_optionText_${index}`}>{option.text}</OptionText>
          </Option>
        ))}
      </Content>
    </Container>
  );
};

export default GlobalAlert;
