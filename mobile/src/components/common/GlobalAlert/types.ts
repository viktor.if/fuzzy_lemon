import { OptionStyle } from '../../../ducks/alert/types';

export type OptionTextProps = {
  type?: OptionStyle;
};
