import styled from 'styled-components/native';
import Text from '../Text';
import { Colors, Typography } from '../../../styles';
import { OptionTextProps } from './types';

export const Container = styled.View`
  align-items: center;
  background-color: ${Colors.backdrop};
  bottom: 0px;
  flex: 1;
  justify-content: center;
  left: 0px;
  position: absolute;
  right: 0px;
  top: 0px;
`;

export const Content = styled.View`
  background-color: ${Colors.white};
  width: 80%;
  border-radius: 12px;
`;

export const Title = styled(Text)`
  font-family: ${Typography.fontFamily};
  font-weight: ${Typography.fontWeight.bold};
  font-size: 20px;
  margin-horizontal: 8px;
  margin-top: 24px;
  text-align: center;
`;

export const Description = styled(Text)`
  font-size: 16px;
  margin-horizontal: 8px;
  margin-bottom: 18px;
  margin-top: -4px;
  text-align: center;
`;

export const Option = styled.Pressable`
  border-color: ${Colors.mercury};
  border-top-width: 1px;
  padding-horizontal: 8px;
  padding-vertical: 8px;
`;

export const OptionText = styled(Text)<OptionTextProps>`
  text-align: center;
  font-size: 20px;
  color: ${({ type }) =>
    type === 'destructive' ? Colors.redOrange : Colors.dodgerBlue};
  font-family: ${Typography.fontFamily};
  font-weight: ${({ type }) =>
    type === 'cancel'
      ? Typography.fontWeight.medium
      : Typography.fontWeight.regular};
`;
