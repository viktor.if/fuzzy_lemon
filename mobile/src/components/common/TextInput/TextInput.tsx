import React, { useState } from 'react';
import { NativeSyntheticEvent, TextInputFocusEventData } from 'react-native';
import SvgIcon from '../SvgIcon';
import {
  Container,
  Input,
  Label,
  ErrorMessage,
  ToggleVisibilityButton,
  DirectionRow,
} from './styled';
import { Props } from './types';
import { Colors } from '../../../styles';

const TextInput = ({
  autoCapitalize,
  autoCompleteType,
  autoCorrect,
  disabled = false,
  error,
  keyboardType,
  label,
  loading = false,
  onBlur,
  onChangeText,
  placeholder,
  textContentType,
  touched = false,
  type,
  value,
}: Props): JSX.Element => {
  const editable = !loading && !disabled;
  const isSecureText = type === 'secure-text';
  const [focused, setFocused] = useState<boolean>(false);
  const [securityOn, setSecurityOn] = useState<boolean>(isSecureText);

  const handleSecurityToggled = () => {
    setSecurityOn((prevState) => !prevState);
  };

  const handleFocused = () => setFocused(true);

  const handleBlured = (e: NativeSyntheticEvent<TextInputFocusEventData>) => {
    onBlur(e);
    setFocused(false);
  };

  return (
    <Container {...{ focused }} invalid={!!error && touched}>
      {error && touched ? (
        <ErrorMessage>{error}</ErrorMessage>
      ) : (
        <Label>{label}</Label>
      )}
      <DirectionRow>
        <Input
          onBlur={handleBlured}
          onFocus={handleFocused}
          placeholderTextColor={Colors.silverChalice}
          secureTextEntry={securityOn}
          selectionColor={Colors.dodgerBlue}
          {...{
            autoCapitalize,
            autoCompleteType,
            autoCorrect,
            editable,
            keyboardType,
            onChangeText,
            placeholder,
            value,
            textContentType,
          }}
        />
        {isSecureText && (
          <ToggleVisibilityButton onPress={handleSecurityToggled}>
            <SvgIcon
              name={securityOn ? 'eye-off' : 'eye'}
              size={24}
              color={Colors.astronaut}
            />
          </ToggleVisibilityButton>
        )}
      </DirectionRow>
    </Container>
  );
};

export default TextInput;
