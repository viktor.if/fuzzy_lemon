import styled from 'styled-components/native';
import { Colors, Typography } from '../../../styles';
import { StyledProps } from './types';
import Text from '../Text';

export const Container = styled.View<StyledProps>`
  align-items: flex-start;
  background-color: ${Colors.transparent};
  border-bottom-width: 2px;
  border-bottom-color: ${({ invalid = false, focused = false }) => {
    if (invalid) return Colors.redOrange;
    if (focused) return Colors.dodgerBlue;
    return Colors.silver;
  }};
  height: 70px;
  justify-content: space-between;
  margin-vertical: 16px;
  overflow: hidden;
`;

export const Label = styled(Text)`
  font-size: 12px;
  color: ${Colors.astronaut};
`;

export const ErrorMessage = styled(Text)`
  font-size: 14px;
  color: ${Colors.redOrange};
`;

export const DirectionRow = styled.View`
  align-items: center;
  flex-direction: row;
  flex: 1;
`;

export const Input = styled.TextInput`
  background-color: ${Colors.transparent};
  color: ${Colors.mineShaft};
  flex: 1;
  font-family: ${Typography.fontFamily};
  font-weight: ${Typography.fontWeight.regular};
  font-size: 16px;
  height: 54px;
  padding-left: 0px;
  width: 100%;
`;

export const ToggleVisibilityButton = styled.Pressable`
  padding: 16px;
`;
