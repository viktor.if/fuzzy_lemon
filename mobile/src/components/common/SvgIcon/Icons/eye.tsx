import React from 'react';
import { Path, G } from 'react-native-svg';
import { ContentProps, SvgSource } from '../types';

export const eye: SvgSource = {
  name: 'eye',
  baseSize: 96,
  Content: ({ scale, color }: ContentProps) => (
    <G
      transform="translate(0.000000,96.000000) scale(0.100000,-0.100000)"
      fill={color}
      {...{ scale }}
    >
      <Path
        d="M360 764 c-93 -25 -162 -64 -231 -133 -101 -102 -158 -241 -101 -249
14 -2 24 3 28 15 24 72 68 146 112 190 224 224 599 148 718 -146 19 -45 27
-56 46 -56 59 0 -1 145 -101 245 -129 127 -306 178 -471 134z"
      />
      <Path
        d="M405 601 c-125 -58 -155 -207 -61 -309 111 -122 326 -35 326 132 0
25 -5 57 -11 73 -24 64 -109 123 -179 123 -19 0 -53 -9 -75 -19z"
      />
    </G>
  ),
};
