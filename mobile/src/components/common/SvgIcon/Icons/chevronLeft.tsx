import React from 'react';
import { Path } from 'react-native-svg';
import { ContentProps, SvgSource } from '../types';

export const chevronLeft: SvgSource = {
  name: 'chevron-left',
  baseSize: 24,
  Content: ({ scale, color }: ContentProps) => (
    <Path
      d="M15.41,16.58L10.83,12L15.41,7.41L14,6L8,12L14,18L15.41,16.58Z"
      fill={color}
      {...{ scale }}
    />
  ),
};
