import React from 'react';
import { Path, G } from 'react-native-svg';
import { ContentProps, SvgSource } from '../types';

export const eyeOff: SvgSource = {
  name: 'eye-off',
  baseSize: 96,
  Content: ({ scale, color }: ContentProps) => (
    <G
      fill={color}
      scale={scale}
      transform="translate(0.000000,96.000000) scale(0.100000,-0.100000)"
    >
      <Path
        d="M456 502 c-380 -381 -404 -410 -352 -420 13 -3 52 29 126 103 l108
108 29 -21 c78 -56 189 -47 249 20 67 73 73 190 13 254 -19 20 -18 20 29 67
l47 47 46 -38 c61 -50 106 -110 135 -179 19 -47 27 -58 46 -58 20 0 23 5 21
33 -2 58 -52 143 -128 218 l-73 72 65 63 c43 43 63 71 61 84 -8 55 -40 29
-422 -353z"
      />
      <Path
        d="M360 764 c-93 -25 -162 -64 -231 -133 -101 -102 -158 -241 -101 -249
14 -2 24 3 28 15 71 210 254 337 460 319 66 -5 73 -4 93 18 l23 24 -52 11
c-68 15 -153 13 -220 -5z"
      />
      <Path
        d="M405 601 c-69 -31 -130 -122 -113 -171 1 -5 48 35 103 90 86 86 96
100 75 100 -14 0 -43 -9 -65 -19z"
      />
    </G>
  ),
};
