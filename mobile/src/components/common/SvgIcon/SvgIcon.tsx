import React from 'react';
import Svg from 'react-native-svg';
import { Props } from './types';
import { IconList } from './data';
import { Colors } from '../../../styles';

const SvgIcon = ({ name, size, color = Colors.black }: Props): JSX.Element => {
  const source = IconList[name];
  const { Content, baseSize } = source;
  const scale = size / baseSize;

  return (
    <Svg width={size} height={size} viewBox={`0 0 ${size} ${size}`}>
      <Content scale={scale} color={color} />
    </Svg>
  );
};

export default SvgIcon;
