import { IconListParams } from './types';
import { chevronLeft } from './Icons/chevronLeft';
import { cogOutline } from './Icons/cogOutline';
import { eye } from './Icons/eye';
import { eyeOff } from './Icons/eyeOff';
import { helpRhombus } from './Icons/helpRhombus';
import { homeOutline } from './Icons/homeOutline';
import { plusCircle } from './Icons/plusCircle';
import { imagePlus } from './Icons/imagePlus';

export const IconList: IconListParams = {
  [chevronLeft.name]: chevronLeft,
  [cogOutline.name]: cogOutline,
  [eye.name]: eye,
  [eyeOff.name]: eyeOff,
  [helpRhombus.name]: helpRhombus,
  [homeOutline.name]: homeOutline,
  [plusCircle.name]: plusCircle,
  [imagePlus.name]: imagePlus,
};
