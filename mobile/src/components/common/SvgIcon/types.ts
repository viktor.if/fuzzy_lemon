export type IconName =
  | 'eye'
  | 'eye-off'
  | 'cog-outline'
  | 'help-rhombus'
  | 'chevron-left'
  | 'plus-circle'
  | 'home-outline'
  | 'image-plus';

export type Props = {
  name: IconName;
  size: number;
  color?: string;
};

export type ContentProps = {
  scale: number;
  color: string;
};

export type SvgSource = {
  name: string;
  baseSize: number;
  Content: (props: ContentProps) => JSX.Element;
};

export type IconListParams = {
  [key: string]: SvgSource;
};
