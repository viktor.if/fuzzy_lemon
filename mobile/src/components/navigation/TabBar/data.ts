import { Pages } from '../../../navigators/Routes';
import { TabBarParamList } from './types';
import { Colors } from '../../../styles';
import i18n from '../../../configs/i18n';

const { PostTimelineScreen, PostEditorScreen, SettingsScreen } = Pages;

export const TabBarList: TabBarParamList = {
  [PostTimelineScreen]: {
    label: i18n.t('home'),
    icon: 'home-outline',
    size: 26,
    tintColorFocused: Colors.scienceBlue,
    tintColorBlulred: Colors.wildBlueYonder,
  },
  [PostEditorScreen]: {
    icon: 'plus-circle',
    size: 56,
    tintColorFocused: Colors.scienceBlue,
    tintColorBlulred: Colors.scienceBlue,
  },
  [SettingsScreen]: {
    label: i18n.t('settings'),
    icon: 'cog-outline',
    size: 26,
    tintColorFocused: Colors.scienceBlue,
    tintColorBlulred: Colors.wildBlueYonder,
  },
};
