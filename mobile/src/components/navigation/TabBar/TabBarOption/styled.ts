import styled from 'styled-components/native';
import Text from '../../../common/Text';
import { Typography } from '../../../../styles';
import { LabelProps } from './types';

export const Container = styled.TouchableOpacity`
  align-items: center;
  flex: 1;
  justify-content: center;
  padding: 4px;
`;

export const Label = styled(Text)<LabelProps>`
  color: ${({ color }) => color}
  font-size: 12px;
  font-family: ${Typography.fontFamily};
  font-weight: ${Typography.fontWeight.bold};
`;
