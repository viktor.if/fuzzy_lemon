import { IconName } from '../../../common/SvgIcon/types';

export type Props = {
  color: string;
  label?: string;
  icon: IconName;
  onPress: () => void;
  size: number;
};

export type LabelProps = {
  color: string;
};
