import React from 'react';
import _ from 'lodash';
import { Container, Label } from './styled';
import SvgIcon from '../../../common/SvgIcon';
import { Props } from './types';

const TabBarOption = ({
  color,
  label,
  icon,
  onPress,
  size,
}: Props): JSX.Element => {
  const showLabel = !_.isUndefined(label);

  return (
    <Container {...{ onPress }}>
      <SvgIcon name={icon} {...{ color, size, onPress }} />
      {showLabel && <Label {...{ color }}>{label}</Label>}
    </Container>
  );
};

export default TabBarOption;
