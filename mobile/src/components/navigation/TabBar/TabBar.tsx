import React from 'react';
import {
  BottomTabBarProps,
  BottomTabBarOptions,
} from '@react-navigation/bottom-tabs';
import { Container } from './styled';
import TabBarOption from './TabBarOption';
import { TabBarList } from './data';

const TabBar = ({
  state,
  navigation,
}: BottomTabBarProps<BottomTabBarOptions>): JSX.Element => {
  const currentRoute = state.routes[state.index].name;

  return (
    <Container>
      {Object.keys(TabBarList).map((route) => {
        const {
          icon,
          label,
          size,
          tintColorBlulred,
          tintColorFocused,
        } = TabBarList[route];
        const isFocused = currentRoute === route;
        const color = isFocused ? tintColorFocused : tintColorBlulred;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route);
          }
        };

        return (
          <TabBarOption
            key={route}
            {...{ color, label, onPress, size, icon }}
          />
        );
      })}
    </Container>
  );
};

export default TabBar;
