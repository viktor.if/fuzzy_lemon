import { IconName } from '../../common/SvgIcon/types';

type TabBarParam = {
  icon: IconName;
  label?: string;
  size: number;
  tintColorFocused: string;
  tintColorBlulred: string;
};

export type TabBarParamList = {
  [key: string]: TabBarParam;
};
