import styled from 'styled-components/native';
import { Colors } from '../../../styles';

export const Container = styled.View`
  flex-direction: row;
  background-color: ${Colors.white};
`;
