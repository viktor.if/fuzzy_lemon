import React from 'react';
import { create } from 'react-test-renderer';
import Button from '../../src/components/common/Button';

describe(('Button'), () => {
  test('should have solid type', () => {
    const tree = create(<Button title='Submit' onPress={() => {}} type='solid' />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('should have clear type', () => {
    const tree = create(<Button title='Submit' onPress={() => {}} type='clear' />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('should have outline type', () => {
    const tree = create(<Button title='Submit' onPress={() => {}} type='outline' />).toJSON();
    expect(tree).toMatchSnapshot();
  });
  
  test('should display loading process', () => {
    const tree = create(<Button title='Submit' onPress={() => {}} type='solid' loading />);
    const activityIndicator = tree.root.findByProps({ 'testID': 'buttonActivity' });
    expect(activityIndicator).toBeTruthy();
  });

  test('should be disabled during loading', () => {
    const tree = create(<Button title='Submit' onPress={() => {}} type='solid' loading />);
    const button = tree.root.findByProps({ 'testID': 'button' }).props;
    expect(button.disabled).toBeTruthy();
  });

  test('should be pressable', () => {
    const tree = create(<Button title='Submit' onPress={() => {}} type='solid' />);
    const button = tree.root.findByProps({ 'testID': 'button' }).props;
    const onPress = jest.fn(button.onPress());

    onPress();

    expect(onPress.mock.calls.length).toBe(1);
  });
});

