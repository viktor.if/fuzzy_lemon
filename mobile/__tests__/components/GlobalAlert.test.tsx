import React from 'react';
import { create } from 'react-test-renderer';

import GlobalAlert from '../../src/components/common/GlobalAlert';
import { Alert } from '../../src/ducks/alert/types';

jest.mock('react-redux', () => {
  const state: Alert  = {
    title: 'text',
    description: 'text',
    options: [          {
      text: 'text',
      style: 'cancel',
      onPress: () => {},
    },]
  };

  const ActualReactRedux = jest.requireActual('react-redux');
  return {
      ...ActualReactRedux,
      useSelector: jest.fn().mockImplementation(() => state),
      useDispatch: jest.fn().mockImplementation(() => () => {}),
      Provider: ({ children }) => children,
  };
});

describe('Global Alert', () => {
  // 1. snapshot
  it('should be rendered', () => {
    const tree = create(<GlobalAlert />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  // 2. props
  it('title should be shown', () => {
    const tree = create(<GlobalAlert />);
    const title = tree.root.findByProps({ 'testID': 'globalAlert_title'}).props;
    expect(title.children).toBe('text');
  });

  it('description should be shown', () => {
    const tree = create(<GlobalAlert />);
    const description = tree.root.findByProps({ 'testID': 'globalAlert_description'}).props;
    expect(description.children).toBe('text');
  });

  it('at least one option should be shown', () => {
    const tree = create(<GlobalAlert />);
    const optionText = tree.root.findByProps({ 'testID': 'globalAlert_optionText_0'}).props;
    expect(optionText.children).toBe('text');
  });

  // 3. types
  it('title should be a string', () => {
    const tree = create(<GlobalAlert />);
    const title = tree.root.findByProps({ 'testID': 'globalAlert_title'}).props;
    expect(typeof title.children).toBe('string');
  });

  it('description should be a string', () => {
    const tree = create(<GlobalAlert />);
    const description = tree.root.findByProps({ 'testID': 'globalAlert_description'}).props;
    expect(typeof description.children).toBe('string');
  });

  it('option text should be a string', () => {
    const tree = create(<GlobalAlert />);
    const optionText = tree.root.findByProps({ 'testID': 'globalAlert_optionText_0'}).props;
    expect(typeof optionText.children).toBe('string');
  });

  // 4. events
  it('option should be pressable', () => {
    const tree = create(<GlobalAlert />);
    const option = tree.root.findByProps({ 'testID': 'globalAlert_option_0'}).props;
    const optionFn = jest.fn(option.onPress);

    optionFn();
    expect(optionFn).toBeCalled();
  });

});