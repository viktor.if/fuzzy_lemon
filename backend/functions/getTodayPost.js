'use strict';

const DateTime = require('../libs/DateTime');
const DynamoDB = require('../libs/DynamoDB');

/**
 * Get a today post
 * @function
 * @param {import('aws-lambda').APIGatewayProxyEvent} [event] Event
 * @returns {Promise}
 */
module.exports.handler = async (event) => {
  /** @type {string} */
  const userId = event.requestContext.authorizer.claims.sub;
  const { Timezone: timezone } =
    /** @type {import('../libs/typedefs').EventHeader} */ event.headers;

  // Validate timezone
  const isTimezoneValid = DateTime.validateTimezone(timezone);
  if (!isTimezoneValid) {
    return {
      statusCode: 400,
      body: JSON.stringify({ msg: 'Invalid timezone' }),
    };
  }

  /** @type {import('../libs/typedefs').Post} */
  let todayPost;

  try {
    todayPost = await DynamoDB.getTodayPost(userId, timezone);
  } catch (error) {
    return {
      statusCode: 500,
      body: JSON.stringify({ msg: 'dynamodb: error occurred' }),
    };
  }

  if (todayPost) {
    return {
      statusCode: 200,
      body: JSON.stringify(todayPost),
    };
  }

  // not found
  return {
    statusCode: 404,
  };
};

// @ts-ignore
if (require.main === module) {
  console.log('/// DEBUGGING');
  exports.handler();
}
