'use strict';

const DynamoDB = require('../libs/DynamoDB');
const S3 = require('../libs/S3');

/**
 * Remove a post
 * @function
 * @param {import('aws-lambda').APIGatewayProxyEvent} [event] Event
 * @returns {Promise}
 */
module.exports.handler = async (event) => {
  /** @type {string} */
  const userId = event.requestContext.authorizer.claims.sub;

  const postId =
    event.pathParameters && event.pathParameters.id
      ? event.pathParameters.id
      : '';
  postId;

  /** @type {import('../libs/typedefs').Post} */
  let removedPost;

  try {
    removedPost = await DynamoDB.removePost(postId, userId);
  } catch (error) {
    if (error instanceof Error && error.message === '404') {
      return {
        statusCode: 404,
      };
    }
    return {
      statusCode: 500,
      body: JSON.stringify({ msg: 'dynamodb: error occurred' }),
    };
  }

  try {
    await S3.removePhoto(removedPost.id);
  } catch {
    return {
      statusCode: 500,
      body: JSON.stringify({ msg: 's3: error occurred' }),
    };
  }

  return {
    statusCode: 204,
  };
};

// @ts-ignore
if (require.main === module) {
  console.log('/// DEBUGGING');
  exports.handler();
}
