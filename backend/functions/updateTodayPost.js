'use strict';

const Parser = require('../libs/Parser');
const DateTime = require('../libs/DateTime');
const DynamoDB = require('../libs/DynamoDB');
const S3 = require('../libs/S3');

/**
 * Update a today post
 * @function
 * @param {import('aws-lambda').APIGatewayProxyEvent} [event] Event
 * @returns {Promise}
 */
module.exports.handler = async (event) => {
  /** @type {string} */
  const userId = event.requestContext.authorizer.claims.sub;
  const { Timezone: timezone } =
    /** @type {import('../libs/typedefs').EventHeader} */ event.headers;

  // Validate timezone
  const isTimezoneValid = DateTime.validateTimezone(timezone);
  if (!isTimezoneValid) {
    return {
      statusCode: 400,
      body: JSON.stringify({ msg: 'Invalid timezone' }),
    };
  }

  // Validate body
  if (event.body === null) {
    return {
      statusCode: 400,
      body: JSON.stringify({ msg: 'Missing body parameters' }),
    };
  }

  // Get today post
  /** @type {import('../libs/typedefs').Post} */
  let todayPost;

  try {
    todayPost = await DynamoDB.getTodayPost(userId, timezone);
  } catch (error) {
    return {
      statusCode: 500,
      body: JSON.stringify({ msg: 'dynamodb: error occurred' }),
    };
  }

  // Check if today post exists
  if (!todayPost) {
    return {
      statusCode: 404,
      body: JSON.stringify({ msg: 'Today post has not been created yet' }),
    };
  }

  const { imageFile, title, description } = await Parser.parsePost(event);

  /** @type {string} */
  let image;

  // Upload image
  if (imageFile) {
    try {
      image = await S3.uploadPhoto(
        imageFile.content,
        imageFile.contentType,
        todayPost.id,
      );
    } catch (error) {
      return {
        statusCode: 500,
        body: JSON.stringify({ msg: 's3: error occurred' }),
      };
    }
  }

  const updatedAt = DateTime.getCurrentDateTimeISO(timezone);

  // Upload updated post
  try {
    await DynamoDB.updatePost(
      todayPost.createdBy,
      todayPost.createdAt,
      title || todayPost.title,
      description || todayPost.description,
      image || todayPost.image,
      updatedAt,
    );
  } catch (error) {
    return {
      statusCode: 500,
      body: JSON.stringify({ msg: 'dynamodb: error occurred' }),
    };
  }

  return {
    statusCode: 204,
  };
};

// @ts-ignore
if (require.main === module) {
  console.log('/// DEBUGGING');
  exports.handler();
}
