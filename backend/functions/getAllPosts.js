'use strict';

const DynamoDB = require('../libs/DynamoDB');

/**
 * Get all posts
 * @function
 * @param {import('aws-lambda').APIGatewayProxyEvent} [event] Event
 * @returns {Promise}
 */
module.exports.handler = async (event) => {
  /** @type {string} */
  const userId = event.requestContext.authorizer.claims.sub;
  /** @type {string} */
  let search;
  /** @type {number} */
  let offset;
  /** @type {number} */
  let limit;

  const queryParams =
    /** @type {import('../libs/typedefs').GetAllPostsQueryParams} */ (
      event.queryStringParameters
    );

  if (queryParams) {
    search = queryParams.search;
    offset = queryParams.offset !== undefined && +queryParams.offset;
    limit = queryParams.limit !== undefined && +queryParams.limit;
  }

  /** @type {import('../libs/typedefs').Post[]} */
  let posts = [];
  let total = 0;

  try {
    posts = await DynamoDB.getAllPosts(userId);
  } catch {
    return {
      statusCode: 500,
      body: JSON.stringify({ msg: 'dynamodb: error occurred' }),
    };
  }

  // filter by a search query
  if (search) {
    posts = posts.filter((post) => {
      const regex = new RegExp(search, 'gi');
      return regex.test(post.title) || regex.test(post.description);
    });
  }

  total = posts.length;

  // extract a page
  if (Number.isInteger(limit) && Number.isInteger(offset)) {
    const limitClipped = limit <= 100 ? limit : 100;
    posts = posts.slice(offset, limitClipped);
  }

  return {
    statusCode: 200,
    body: JSON.stringify({
      data: posts,
      total,
    }),
  };
};

// @ts-ignore
if (require.main === module) {
  console.log('/// DEBUGGING');
  exports.handler();
}
