'use strict';

const DateTime = require('../libs/DateTime');
const DynamoDB = require('../libs/DynamoDB');
const Parser = require('../libs/Parser');
const Post = require('../libs/Post');
const S3 = require('../libs/S3');
const { v4: uuidv4 } = require('uuid');

/**
 * Create a today post
 * @function
 * @param {import('aws-lambda').APIGatewayProxyEvent} [event] Event
 * @returns {Promise}
 */
module.exports.handler = async (event) => {
  /** @type {string} */
  const userId = event.requestContext.authorizer.claims.sub;
  const { Timezone: timezone } =
    /** @type {import('../libs/typedefs').EventHeader} */ event.headers;

  // validate timezone
  const isTimezoneValid = DateTime.validateTimezone(timezone);
  if (!isTimezoneValid) {
    return {
      statusCode: 400,
      body: JSON.stringify({ msg: 'Invalid timezone' }),
    };
  }

  const { imageFile, title, description } = await Parser.parsePost(event);

  // validate parameters
  if (
    [
      imageFile === undefined,
      title === undefined,
      description === undefined,
    ].includes(true)
  ) {
    return {
      statusCode: 400,
      body: JSON.stringify({ msg: 'Missing body parameters' }),
    };
  }

  const dateTimeISO = DateTime.getCurrentDateTimeISO(timezone);

  // check if today post is already created
  const isTodayPost = await Post.isTodayPostCreated(userId, timezone);

  if (isTodayPost instanceof Error) {
    return {
      statusCode: 500,
    };
  } else if (isTodayPost) {
    return {
      statusCode: 400,
      body: JSON.stringify({ msg: 'Today post already exists' }),
    };
  }

  const postId = uuidv4();

  // upload image
  let image;
  try {
    image = await S3.uploadPhoto(
      imageFile.content,
      imageFile.contentType,
      postId,
    );
  } catch (error) {
    return {
      statusCode: 500,
      body: JSON.stringify({ msg: 's3: error occurred' }),
    };
  }

  /** @type {import('../libs/typedefs').Post} */
  const post = {
    createdAt: dateTimeISO,
    createdBy: userId,
    description: description,
    id: postId,
    image: image,
    title: title,
    updatedAt: dateTimeISO,
  };

  // put a post into the DB
  try {
    await DynamoDB.uploadPost(post);
  } catch (error) {
    return {
      statusCode: 500,
      body: JSON.stringify({ msg: 'dynamodb: error occurred' }),
    };
  }

  return {
    statusCode: 200,
    body: JSON.stringify(post),
  };
};

// @ts-ignore
if (require.main === module) {
  console.log('/// DEBUGGING');
  exports.handler();
}
