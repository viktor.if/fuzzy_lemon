'use strict';

const AWS = require('aws-sdk');
AWS.config.update({
  region: process.env.REGION,
  apiVersion: '2012-08-10',
});

const DateTime = require('./DateTime');
const docClient = new AWS.DynamoDB.DocumentClient();

module.exports = {
  /**
   * Uploads a post to the db
   * @param {import('./typedefs').Post} post Post
   */
  async uploadPost(post) {
    const params = {
      TableName: process.env.DYNAMODB_TABLE_POSTS,
      Item: post,
    };

    await docClient.put(params).promise();
  },

  /**
   * Returns all posts from the db
   * @param {string} userId User id
   * @returns {Promise<import('./typedefs').Post[]>} Array of posts
   */
  async getAllPosts(userId) {
    const params = {
      TableName: process.env.DYNAMODB_TABLE_POSTS,
      KeyConditionExpression: '#createdBy = :userId',
      ExpressionAttributeNames: {
        '#createdBy': 'createdBy',
      },
      ExpressionAttributeValues: {
        ':userId': userId,
      },
      ScanIndexForward: false,
    };

    /** @type {import('./typedefs').Post[]} */
    let posts = [];
    /** @type {AWS.DynamoDB.Key} */
    let lastEvaluatedKey = null;

    do {
      if (lastEvaluatedKey) {
        params.ExclusiveStartKey = lastEvaluatedKey;
      }

      await docClient
        .query(params, (error, data) => {
          if (error) {
            throw error;
          } else {
            const { LastEvaluatedKey } = data;
            const Items = /** @type {import('./typedefs').Post[]} */ (
              data.Items
            );

            posts = [...posts, ...Items];
            lastEvaluatedKey = LastEvaluatedKey || null;
          }
        })
        .promise();
    } while (lastEvaluatedKey);

    return posts;
  },

  /**
   * Returns a today post
   * @param {string} userId User id
   * @param {string} timezone Timezone
   * @returns {Promise<import('./typedefs').Post | null>} Post
   */
  async getTodayPost(userId, timezone) {
    const params = {
      TableName: process.env.DYNAMODB_TABLE_POSTS || 'fjord-posts-dev',
      KeyConditionExpression: '#createdBy = :userId',
      ExpressionAttributeNames: {
        '#createdBy': 'createdBy',
      },
      ExpressionAttributeValues: {
        ':userId': userId,
      },
      ScanIndexForward: false,
    };

    /** @type {import('./typedefs').Post[]} */
    let posts = [];

    await docClient
      .query(params, (error, data) => {
        if (error) {
          throw error;
        } else {
          const Items = /** @type {import('./typedefs').Post[]} */ (data.Items);
          posts = Items;
        }
      })
      .promise();

    if (
      posts[0] &&
      DateTime.getCurrentDateTime(timezone) ===
        DateTime.getDateTimeFromISO(posts[0].createdAt)
    ) {
      return posts[0];
    }

    return null;
  },

  /**
   * Updates a post
   * @param {string} createdBy Owner id
   * @param {string} createdAt Date of creation
   * @param {string} [title] Title
   * @param {string} [description] Desription
   * @param {string} [image] Image URL
   * @param {string} updatedAt Update time
   * @returns {Promise}
   */
  async updatePost(createdBy, createdAt, title, description, image, updatedAt) {
    const params = {
      TableName: process.env.DYNAMODB_TABLE_POSTS,
      Key: { createdBy, createdAt },
      UpdateExpression:
        'set updatedAt = :updatedAt, title = :title, description = :description, image = :image',
      ExpressionAttributeValues: {
        ':updatedAt': updatedAt,
        ':title': title,
        ':description': description,
        ':image': image,
      },
      ReturnValues: 'UPDATED_NEW',
    };

    await docClient
      .update(params, (error) => {
        if (error) {
          throw error;
        }
      })
      .promise();
  },

  /**
   * Removes a post
   * @param {string} postId Post id
   * @param {string} userId User id
   * @returns {Promise<import('./typedefs').Post>}
   */
  async removePost(postId, userId) {
    const posts = await this.getAllPosts(userId);
    const post = posts.find((item) => item.id === postId);

    if (post) {
      const params = {
        TableName: process.env.DYNAMODB_TABLE_POSTS,
        Key: {
          createdBy: userId,
          createdAt: post.createdAt,
        },
      };

      await docClient.delete(params).promise();
      return post;
    }

    throw new Error('404');
  },
};
