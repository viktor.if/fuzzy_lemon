'use strict';

const AWS = require('aws-sdk');
const s3 = new AWS.S3();

module.exports = {
  /**
   * Writes a photo of the post to the S3
   * @param {Buffer} image Base64 encoded image
   * @param {string} [type] Content type
   * @param {string} postId Post id
   * @returns {Promise<string>} Image URL
   */
  async uploadPhoto(image, type = 'image', postId) {
    const imageName = `images/${postId}.jpeg`;
    const params = {
      Bucket: process.env.S3_BUCKET_POSTS,
      Key: imageName,
      Body: image,
      ContentType: type,
    };

    const uploadedImage = await s3.upload(params).promise();
    return uploadedImage.Location;
  },

  /**
   * Removes an image from the bucket
   * @param {string} postId Post id
   * @returns {Promise}
   */
  async removePhoto(postId) {
    const imageName = `images/${postId}.jpeg`;
    const params = {
      Bucket: process.env.S3_BUCKET_POSTS,
      Key: imageName,
    };

    await s3.deleteObject(params).promise();
  },
};
