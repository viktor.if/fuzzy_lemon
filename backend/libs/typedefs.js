/**
 * Post type
 * @typedef {Object} Post
 * @property {string} id ID
 * @property {string} title Title
 * @property {string} description Description
 * @property {string} image Image URL
 * @property {string} createdBy Author
 * @property {string} createdAt Time of creation
 * @property {string} updatedAt Time of update
 */

/**
 * Event header with timezone
 * @typedef {Object} EventHeader
 * @property {string} Timezone Timezone
 */

/**
 * Query params to get all posts
 * @typedef GetAllPostsQueryParams
 * @property {string | undefined} search Search
 * @property {string | undefined} offset Offset
 * @property {string | undefined} limit Limitr
 */

/**
 * Multipart file type
 * @typedef MultipartFile
 * @property {string} filename File name
 * @property {Buffer} content Content buffer
 * @property {string} contentType Content tuye
 * @property {string} encoding Encoding
 * @property {string} fieldname Field name
 */

/**
 * Multypart body type
 * @typedef MultipartBody
 * @property {MultipartFile[]} files Files
 * @property {string} title Title
 * @property {string} description Description
 */

exports.unused = {};
