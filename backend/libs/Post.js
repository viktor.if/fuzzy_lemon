'use strict';

const DateTime = require('./DateTime');
const DynamoDB = require('./DynamoDB');

module.exports = {
  /**
   * Checks if today post has been already created
   * @param {string} userId
   * @param {string} timezone
   * @returns {Promise<boolean | Error>}
   */
  async isTodayPostCreated(userId, timezone) {
    /** @type {import('./typedefs').Post[]} */
    let posts = await DynamoDB.getAllPosts(userId);

    if (
      posts.length &&
      DateTime.getCurrentDateTime(timezone) ===
        DateTime.getDateTimeFromISO(posts[0].createdAt)
    ) {
      return true;
    }

    return false;
  },
};
