'use strict';

const { DateTime, IANAZone } = require('luxon');
const DEFAULT_DATETIME_FORMAT = 'yyyy-LLL-dd';

module.exports = {
  /**
   * Validates timezone
   * @param {string} timezone - Timezone
   * @returns {boolean}
   */
  validateTimezone(timezone) {
    return IANAZone.create(timezone).isValid;
  },

  /**
   * Converts datetime from ISO into new format
   * @param {string} datetimeISO - ISO datetime
   * @param {string} [format] - Datetime format to convert into
   * @returns {string} - Converted datetime
   */
  getDateTimeFromISO(datetimeISO, format = DEFAULT_DATETIME_FORMAT) {
    return DateTime.fromISO(datetimeISO, { setZone: true }).toFormat(format);
  },

  /**
   * Returns current datetime in provided format
   * @param {string} timezone - Timezone
   * @param {string} [format] - Format to convert datetime into
   * @returns {string} - Current datetime
   */
  getCurrentDateTime(timezone, format = DEFAULT_DATETIME_FORMAT) {
    return DateTime.utc().setZone(timezone).toFormat(format);
  },

  /**
   * Returns current datetime in ISO format
   * @param {string} timezone - Timezone
   * @returns {string} - Current datetime
   */
  getCurrentDateTimeISO(timezone) {
    return DateTime.utc().setZone(timezone).toISO({ includeOffset: true });
  },
};
