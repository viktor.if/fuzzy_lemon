'use strict';

const Parser = require('lambda-multipart-parser');

/**
 * Multipart parser
 * @param {import('aws-lambda').APIGatewayProxyEvent} event
 * @return {Promise}
 */
module.exports = {
  async parsePost(event) {
    const multipart = /** @type {unknown} */ (await Parser.parse(event));
    const { title, description, files } =
      /** @type {import('./typedefs').MultipartBody} */ (multipart);

    return { title, description, imageFile: files[0] };
  },
};
