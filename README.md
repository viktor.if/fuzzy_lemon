# Fuzzy Lemon

Photo memories

[![Platform - Android](https://img.shields.io/badge/platform-Android-3ddc84.svg?style=flat&logo=android)](https://www.android.com)
[![Platform - iOS](https://img.shields.io/badge/platform-iOS-000.svg?style=flat&logo=apple)](https://developer.apple.com/ios)

[![Code style: Prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

## Development environment

- React Native (0.63.4)
- React (16.13.1)
- Serverless / AWS

## AWS services

- API Gateway
- Cognito
- DynamoDB
- IAM
- Lambda
- S3

## Basic Serverless CLI commands

- Deploy the project

```bash
sls deploy --aws-profile <profile> --stage <stage>
```

- Remove the project

```bash
sls remove --aws-profile <profile> --stage <stage>
```

- Upload a function

```bash
sls deploy --aws-profile <profile> --stage <stage> -f <function>
```
